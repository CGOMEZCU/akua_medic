class Mensajes {
    static showSuccess(body, title) {
        toastr.success(body, title, {
            "progressBar": true,
            "positionClass": "toast-bottom-right"
        })
    }

    static showError(body, title) {
        toastr.error(body, title, {
            "progressBar": true,
            "positionClass": "toast-bottom-right"
        })
    }

    static showError(body, title) {
        toastr.error(body, title, {
            "progressBar": true,
            "positionClass": "toast-bottom-right"
        })
    }


    static showWarning(body, title) {
        toastr.warning(body, title, {
            "progressBar": true,
            "positionClass": "toast-bottom-right"
        })
    }
}


class Util {

    static addEventErrorInput(element) {
        $("#" + element)
            .closest('.form-group')
            .removeClass('has-success')
            .addClass('has-error')
    }
    static removeEventErrorInput(element) {
        $("#" + element)
            .closest('.form-group')
            .removeClass('has-error')
            .addClass('has-success')
    }

    static isEmty(value) {
        var status = false;
        if (value == null || value == "") {
            status = true;
        }
        return status;
    }

    static checkEmptyFields(element) {
        var value = $("#" + element).val();
        if (this.isEmty(value)) {
            this.addEventErrorInput(element);
            return 0;
        } else {
            this.removeEventErrorInput(element);
            return 1;
        }
        return 1;
    }

    static convertDateToFormatDia(fecha) {
        let date = new Date(fecha.replace(/-+/g, '/'));
        let options = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };

        return date.toLocaleDateString('es-MX', options);
    }

    static getMaxHour(array) {
        var max = array[0];
        array.forEach(function (element) {
            //del arreglo
            var arreglo1_1 = element.split(' ');
            var arreglo2_1 = arreglo1_1[0].split(':');

            var minutes_1 = arreglo2_1[1];
            var hour_1 = arreglo2_1[0];
            if (arreglo1_1[1] == 'PM') {
                var hour_1_aux = parseInt(hour_1);
                if (hour_1_aux < 12) {
                    hour_1 = 12 + hour_1_aux;
                }
            }


            //comparamos con la fecha mas maxima que se haya registrado
            var arreglo1_2 = max.split(' ');
            var arreglo2_2 = arreglo1_2[0].split(':');

            var minutes_2 = arreglo2_2[1];
            var hour_2 = arreglo2_2[0];

            if (arreglo1_2[1] == 'PM') {
                //si es pm entonnces lo que se hace es sumar +12 al numero
                //ejemplo 1 enteonces se sumaria =12+1 para que sea las 13

                var hour_2_aux = parseInt(hour_2);
                if (hour_2_aux < 12) {
                    hour_2 = 12 + hour_2_aux;
                }

            }
            //ejemplo se ha seleccionado 
            //9:00= 900
            //10:00=100
            //9:30= 930           
            //lo que se hace es quitar el : y unirlo para que dea un solo numero
            //luego se esta verificando el mayor de cada uno y se va asignando ala variable max


            var ajuntado1 = hour_1 + "" + minutes_1;
            var ajuntado2 = hour_2 + "" + minutes_2;
            ajuntado1 = parseInt(ajuntado1);
            ajuntado2 = parseInt(ajuntado2);
            if (ajuntado1 >= ajuntado2) {
                max = element;
            }
        });
        return max;
    }
    static getMinHour(array) {
        var min = array[0];
        array.forEach(function (element) {
            //del arreglo
            var arreglo1_1 = element.split(' ');
            var arreglo2_1 = arreglo1_1[0].split(':');

            var minutes_1 = arreglo2_1[1];
            var hour_1 = arreglo2_1[0];
            if (arreglo1_1[1] == 'PM') {
                var hour_1_aux = parseInt(hour_1);
                if (hour_1_aux < 12) {
                    hour_1 = 12 + hour_1_aux;
                }
            }

            //comparamos con la fecha mas maxima que se haya registrado
            var arreglo1_2 = min.split(' ');
            var arreglo2_2 = arreglo1_2[0].split(':');

            var minutes_2 = arreglo2_2[1];
            var hour_2 = arreglo2_2[0];
            if (arreglo1_2[1] == 'PM') {

                var hour_2_aux = parseInt(hour_2);
                if (hour_2_aux < 12) {
                    hour_2 = 12 + hour_2_aux;
                }

            }
            var ajuntado1 = hour_1 + "" + minutes_1;
            var ajuntado2 = hour_2 + "" + minutes_2;
            ajuntado1 = parseInt(ajuntado1);
            ajuntado2 = parseInt(ajuntado2);
            if (ajuntado1 <= ajuntado2) {
                min = element;
            }
        });
        return min;
    }

    static checkedToRadiobutton(array, name, state, indice) {
        var i = 0;
        i = parseInt(i);
        indice = parseInt(indice);
        array.forEach(function (element) {
            if (i != indice) {
                $("#" + name + "-" + i).prop("checked", false);
            }
            i = parseInt(i) + 1;

        });
    }


    static formatDate(date) {
        var date_aux = new Date(date);
        date_aux = moment(date_aux).format('YYYY-MM-DD');
        return date_aux;
    }
    static formatDateWithHour(date) {
        var date = new Date(start_Date);
        var date = moment(date).format('YYYY-MM-DD');
    }

}