
'use strict'
var arrayAppointments = null
var titleYears = []
var dataAtended = []
var dataAbsent = []
// month
var dataMonthAttended = []
var dataMonthAbsent = []

var titleMonth = ['01','02','03','04','05','06','07','08','09','10','11','12']

function loadReport (appointments, arrayYears) {

  var e = new Date,
  year = e.getFullYear(),
  r = {};
  arrayAppointments = appointments
  arrayYears.forEach(function (element) {
    titleYears.push(element.anio)
    dataAtended.push(0)
    dataAbsent.push(0)
    document.getElementById('cbo_reportAnio').innerHTML +="<option value='" + element.anio + "'>" + element.anio + '</option>'
  })
  

   getAppointmentForAnio() 
  getAppointmentsForMonths(year)
}
function getAppointmentForAnio () {
  arrayAppointments.forEach(function (elementAppointments) {
    titleYears.forEach(function (elementYear) {
      console.log(elementAppointments.state);
      if (elementAppointments.state == 'Atendido') {
        if (elementYear == elementAppointments.anio) {
          var valor = getNumberAnio(elementAppointments.anio)
          dataAtended[valor] = parseInt(dataAtended[valor]) + 1
        }
      } else if (elementAppointments.state == 'Ausente') {
        if (elementYear == elementAppointments.anio) {
          var valor = getNumberAnio(elementAppointments.anio)
          dataAbsent[valor] = parseInt(dataAbsent[valor]) + 1
        }
      }
    })
  })
  initReportAnio()
}

function getAppointmentsForMonths (anio) {
  dataMonthAttended=[];
  dataMonthAbsent=[];
  for (var i = 1; i <= 12; i++) {
    dataMonthAttended.push(0)
    dataMonthAbsent.push(0)
  }
  arrayAppointments.forEach(function (elementAppointments) {
    titleMonth.forEach(function (elementMonth) {
      if (elementAppointments.state == 'Atendido') {
        if (
          elementMonth == elementAppointments.month &&
          elementAppointments.anio == anio
        ) {
          var valor = getPositionMonth(elementAppointments.month)
          dataMonthAttended[valor] = parseInt(dataMonthAttended[valor]) + 1
        }
      } else if (
        elementAppointments.state == 'Ausente' &&
        elementAppointments.anio == anio
      ) {
        if (elementMonth == elementAppointments.month) {
          var valor = getPositionMonth(elementAppointments.month)
          dataMonthAbsent[valor] = parseInt(dataMonthAbsent[valor]) + 1
        }
      }
    })
  })
   initReportMonth() 
}

function getPositionMonth (numberMonth) {
  var contador = 0
  var valor = -1
  titleMonth.forEach(function (element) {
    if (element == numberMonth) {
      valor = contador
    }
    contador = parseInt(contador) + 1
  })

  return valor
}

function getNumberAnio (anio) {
  var contador = 0
  var valor = -1
  titleYears.forEach(function (element) {
    if (element == anio) {
      valor = contador
    }
    contador = parseInt(contador) + 1
  })
  return valor
}

function initReportAnio () {
  new Chart(document.getElementById('bar-chart'), {
    type: 'bar',
    data: {
      labels: titleYears,
      datasets: [
        {
          label: 'Atendidos',
          backgroundColor: '#3e95cd',
          data: dataAtended
        },
        {
          label: 'Ausentes',
          backgroundColor: '#8e5ea2',
          data: dataAbsent
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'REPORTE DE CITAS POR AÑO'
      }
    }
  })
}

function initReportMonth () {
  var color = Chart.helpers.color
  var barChartData = {
    labels: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sept','Oct','Nov','Dic'],
    datasets: [
      {
        type: 'bar',
        label: 'Atendidos',
        backgroundColor: color(window.chartColors.red)
          .alpha(0.2)
          .rgbString(),
        borderColor: window.chartColors.red,
        data: dataMonthAttended
      },
      {
        type: 'line',
        label: 'Ausentes',
        backgroundColor: color(window.chartColors.blue)
          .alpha(0.2)
          .rgbString(),
        borderColor: window.chartColors.blue,
        data: dataMonthAbsent
      }
    ]
  }

  var ctx = document.getElementById('canvas1').getContext('2d')
    ctx.innerHTML='';
  window.myBar = new Chart(ctx, {
    type: 'bar',
    data: barChartData,
    options: {
      responsive: true,
      title: {
        display: true,
        text: 'REPORTE DE CITAS POR MES'
      }
    }
  })
}

// EVENTOS



$("#cbo_reportAnio").on("change", function(value){
  var numberSelected = $(this).val();  
  var numberSelected=document.getElementById('cbo_reportAnio').value;
 
  try {
    getAppointmentsForMonths(numberSelected);
  }
  catch(error) {
    console.error(error);
    // expected output: ReferenceError: nonExistentFunction is not defined
    // Note - error messages will vary depending on browser
  }
});