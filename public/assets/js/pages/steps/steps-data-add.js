
"use strict";

//declaraciones de variables
var form = $("#example-advanced-form").show();

//A trabjar

//doctor selected
var doctor_selected = null;
var patient_selected = null;
var hour_start_selected = null;
var hour_end_selected = null;
var date_selected = null;
var description_selected = null;
var cost_selected = null;
var array_doctors = null;


var checkbox_interval = false;
var array_intervalor = [];


function option1() {
    if (Util.isEmty(date_selected)) {
        Util.addEventErrorInput('start_Date_id');
        state = false;
    } else {
        var btn_verifyDayAttention = document.getElementById('btn-verify-Day-Attention')
        var btn_verifyDayAttention = btn_verifyDayAttention.getAttribute('href')

        var data = Util.formatDate(date_selected);
        var state = true;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                    'content'
                )
            },
            type: 'GET',
            dataType: 'json',
            url: btn_verifyDayAttention,
            async: false,
            data: { data: data },
            success: function (resultado) {
                if (resultado.state == "Correcto") {
                    var date_aux = $("#start_Date_id").val();
                    $("#id-date-Selected-label").html("Cita para la fecha " + date_aux);
                    Util.removeEventErrorInput('start_Date_id');
                    state = true;
                } else {
                    Mensajes.showWarning(resultado.response, "Cita");
                    state = false;
                }

            }
        })

    }
    return state;
}

function option2() {
    var btn_generateHourAttention = document.getElementById('btn_generateHourAttention')
    var btn_generateHourAttention = btn_generateHourAttention.getAttribute('href')
    var date = Util.formatDate(date_selected);
    var data = doctor_selected.id + " " + date;
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                'content'
            )
        },
        type: 'GET',
        dataType: 'json',
        url: btn_generateHourAttention,
        async: false,
        data: { data: data },
        success: function (resultado) {
            if (resultado.state == "Correcto") {
                cost_selected = resultado.price_appointments;
                $("#text-price").val(cost_selected);
                paintCalendar(resultado.array_calendar, resultado.array_horas_solicitadas_registradas);
            } else {
                Mensajes.showWarning(resultado.response, "Horario");
            }

        }
    })
}

function option3() {
    if (checkbox_interval) {
        if (array_intervalor.length > 0) {
            return true;
        } else {
            return false;
        }
    } else if (Util.isEmty(hour_start_selected)) {
        return false;
    } else {
        return true;
    }
}

function option4() {
    var count = 0;//contador
    var adder = 0;//sumador

    count = Util.checkEmptyFields('text-description');
    adder = parseInt(adder) + parseInt(count);

    count = Util.checkEmptyFields('text-price');
    adder = parseInt(adder) + parseInt(count);
    return adder == 2;
}

function option5() {
    //datos para guardar
    $("#doctor_id").val(doctor_selected.id);
    $("#description").val($("#text-description").val());

    var date = Util.formatDate(date_selected);
    if (checkbox_interval) {
        $("#start_Date").val(date + " " + Util.getMinHour(array_intervalor));
        $("#end_Date").val(date + " " + Util.getMaxHour(array_intervalor));
    } else {
        $("#start_Date").val(date + " " + hour_start_selected);
    }

    $("#price").val($("#text-price").val());
    $("#standbyTime").val(doctor_selected.standbyTime);


    //para mostrar al usuario
    $("#verify-patient").val(patient.patient_name);
    $("#verify-hour").val(date + " " + hour_start_selected);
    $("#verify-doctor").val(doctor_selected.name_doctor);
    $("#verify-speciality").val(doctor_selected.name_speciality);
    $("#verify-description").val($("#text-description").val());
    $("#verify-price").val($("#text-price").val());

}

function save() {
    var btn_add = document.getElementById("btn-add");
    btn_add.click();
}


function paintSpesificRadioButton(element, i, container) {
    container.innerHTML += `
    <div class="col-lg-12 p-t-20"> 
    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="option-${i}">
        <input type="checkbox" id="option-${i}" class="mdl-checkbox__input" >
        <span class="mdl-checkbox__label" style="color: black;">${element}</span>
      </label>
  </div>
                            `
}
function paintSpesificCheckBox(element, container) {
    container.innerHTML += `
    <div class="col-lg-12 p-t-20"> 
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1">
                          <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input" checked disabled>
                          <span class="mdl-checkbox__label" style="color: blue;">${element}</span>
                        </label>
                    </div>
    `
}

function paintCalendar(array_calendar, array_horas_solicitadas_registradas) {
    console.log(array_calendar)
    console.log(array_horas_solicitadas_registradas)
    var divido = array_calendar.length / 3;
    divido = parseInt(divido);
    var contador = 0;
    var sumador = 1;
    contador = parseInt(contador);
    sumador = parseInt(sumador);
    var container_calendar_1 = document.getElementById('container-calendar-1');
    var container_calendar_2 = document.getElementById('container-calendar-2');
    var container_calendar_3 = document.getElementById('container-calendar-3');
    /*  var container_calendar_4 = document.getElementById('container-calendar-4'); */
    container_calendar_1.innerHTML = "";
    container_calendar_2.innerHTML = "";
    container_calendar_3.innerHTML = "";
    /* container_calendar_4.innerHTML = ""; */
    var i = 0;
    for (i = 0; i < array_calendar.length; i++) {
        var element = array_calendar[i];
        var elementOriginal = array_calendar[i];
        if (array_calendar[i + 1] != null) {
            element = element + " - " + array_calendar[i + 1];
        }

        if (contador < divido) {
            switch (sumador) {
                case 1:
                    if (verifyHourSelected(elementOriginal, array_horas_solicitadas_registradas)) {
                        paintSpesificRadioButton(element, i, container_calendar_1);
                    } else {
                        paintSpesificCheckBox(element, container_calendar_1);
                    }
                    contador++;
                    break;

                case 2:
                    if (verifyHourSelected(elementOriginal, array_horas_solicitadas_registradas)) {
                        paintSpesificRadioButton(element, i, container_calendar_2);
                    } else {
                        paintSpesificCheckBox(element, container_calendar_2);
                    }
                    contador++;
                    break;
                case 3:
                    if (verifyHourSelected(elementOriginal, array_horas_solicitadas_registradas)) {
                        paintSpesificRadioButton(element, i, container_calendar_3);
                    } else {
                        paintSpesificCheckBox(element, container_calendar_3);
                    }
                    contador++;
                    break;
                /* case 4:
                    if (verifyHourSelected(elementOriginal, array_horas_solicitadas_registradas)) {
                        paintSpesificRadioButton(element, i, container_calendar_4);
                    } else {
                        paintSpesificCheckBox(element, container_calendar_4);
                    }
                    contador++;
                    break; */
            }

        } else {
            switch (sumador) {
                case 1:
                    if (verifyHourSelected(elementOriginal, array_horas_solicitadas_registradas)) {
                        paintSpesificRadioButton(element, i, container_calendar_1);
                    } else {
                        paintSpesificCheckBox(element, container_calendar_1);
                    }

                    break;
                case 2:
                    if (verifyHourSelected(elementOriginal, array_horas_solicitadas_registradas)) {
                        paintSpesificRadioButton(element, i, container_calendar_2);
                    } else {
                        paintSpesificCheckBox(element, container_calendar_2);
                    }
                    break;
                case 3:
                    if (verifyHourSelected(elementOriginal, array_horas_solicitadas_registradas)) {
                        paintSpesificRadioButton(element, i, container_calendar_3);
                    } else {
                        paintSpesificCheckBox(element, container_calendar_3);
                    }
                    break;
                /* case 4:
                    if (verifyHourSelected(elementOriginal, array_horas_solicitadas_registradas)) {
                        paintSpesificRadioButton(element, i, container_calendar_4);
                    } else {
                        paintSpesificCheckBox(element, container_calendar_4);
                    }
                    break; */
            }
            sumador++;
            contador = 0;
        }

    }
    listenerRadioButton(array_calendar);

}

function verifyHourSelected(hour, arrayHourSelected) {
    var state = true;
    arrayHourSelected.forEach(function (element) {
        if (element == hour) {
            state = false;
        }
    });
    return state;
}

function addToInterval(element) {
    array_intervalor.push(element);
}

function deleteToInterval(element) {
    var i = 0;
    for (i = 0; i < array_intervalor.length; i++) {
        if (element == array_intervalor[i]) {
            array_intervalor.splice(i, 1);
        }
    }
}

function listenerRadioButton(array) {
    var i = 0;
    i = parseInt(i);
    array.forEach(function (element) {
        $('#option-' + i).on('click', function () {
            if ($(this).is(':checked')) {
                // Hacer algo si el checkbox ha sido seleccionado
                var valor = $(this).attr("id");
                var array_aux = valor.split("-");
                hour_start_selected = element;

                //verificamos si esta el intervalor seleccionado
                if (checkbox_interval) {
                    addToInterval(element);
                } else {
                    Util.checkedToRadiobutton(array, "option", false, array_aux[1]);
                }
            } else {
                // Hacer algo si el checkbox ha sido deseleccionado
                if (checkbox_interval) {
                    deleteToInterval(element);
                }
                hour_start_selected = null;
            }

        });
        i = parseInt(i) + 1;
    });

    $('#checkbox-interval').on('click', function () {
        if ($(this).is(':checked')) {
            Util.checkedToRadiobutton(array, "option", false, -1);
        } else {
            Util.checkedToRadiobutton(array, "option", false, -1);
        }

    });
}


var steps_data = (function () {
    return {
        init: function (doctors, patient, start_Dates) {
            patient_selected = patient;
            array_doctors = doctors;
            if (start_Dates != null) {
                $("#start_Date_id").val(moment(start_Dates).format('YYYY-MM-DD'));
                date_selected = moment(start_Dates).format('YYYY-MM-DD HH:mm');
            }

            array_doctors.forEach(function (element) {
                $('#checkbox-' + element.slug).on('click', function () {
                    if ($(this).is(':checked')) {
                        // Hacer algo si el checkbox ha sido seleccionado
                        doctor_selected = element;
                    } else {
                        // Hacer algo si el checkbox ha sido deseleccionado
                        doctor_selected = null;
                    }
                });
            });

            $('#checkbox-interval').on('click', function () {
                if ($(this).is(':checked')) {
                    // Hacer algo si el checkbox ha sido seleccionado
                    checkbox_interval = true;
                } else {
                    // Hacer algo si el checkbox ha sido deseleccionado
                    checkbox_interval = false;
                }

            });
        }
    }
})()

form.steps({
    headerTag: "h3",
    bodyTag: "fieldset",
    transitionEffect: "slideLeft",
    stepsOrientation: "vertical",
    onStepChanging: function (event, currentIndex, newIndex) {
        //verificamos si se ha seleccionado un docto 
        if (parseInt(newIndex) == 1) {
            var status = option1();
            if (status) {
                return true;
            } else {
                return false;
            }
        }

        if (parseInt(newIndex) == 2) {
            if (!Util.isEmty(doctor_selected)) {
                option2();
                return true;
            } else {
                return false;
            }
        }
        if (parseInt(newIndex) == 3) {
            var status = option3();
            if (status) {
                return true;
            } else {
                return false;
            }
        }

        if (parseInt(newIndex) == 4) {
            if (option4()) {
                option5();
                return true;
            } else {
                return false;
            }
        }


        // Allways allow previous action even if the current form is not valid!
        if (currentIndex > newIndex) {
            return true;
        }

        // Needed in some cases if the user went back (clean up)
        if (currentIndex < newIndex) {
            // To remove error styles
            form.find(".body:eq(" + newIndex + ") label.error").remove();
            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        }
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
    },
    onStepChanged: function (event, currentIndex, priorIndex) {

        // Used to skip the "Warning" step if the user is old enough.
        if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
            form.steps("next");
        }
        // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
        if (currentIndex === 2 && priorIndex === 3) {
            form.steps("previous");
        }
    },
    onFinishing: function (event, currentIndex) {
        save();
        form.validate().settings.ignore = ":disabled";
        return form.valid();


    },
    onFinished: function (event, currentIndex) {
        /* Mensajes.showWarning('Falta llenar algunos campos', 'Cita'); */
    }
}).validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        confirm: {
            equalTo: "#password-2"
        }
    }
});

$("#example-vertical").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    stepsOrientation: "vertical"
});

$(document).ready(function () {
    $('#start_Date_id').bootstrapMaterialDatePicker
        ({
            format: 'YYYY-MM-DD',
            time: false,
            lang: 'es',
            clearButton: true
        }).on('change', function (e, date) {
            date_selected = new Date(date);
            date_selected = moment(date_selected).format('YYYY-MM-DD HH:mm');
        });



});