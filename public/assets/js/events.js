var users = (function () {
  return {
    init: function () {
      // Validaciones
      var form1 = $('#users_form')

      form1.validate({
        errorElement: 'span', // default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: '', // validate all fields including form hidden inpu
        messages: {
          rol_id: {
            required: 'Campo obligatorio'
          },
          name: {
            required: 'Campo obligatorio'
          },
          email: {
            required: 'Campo obligatorio'
          },
          password: {
            required: 'Campo obligatorio'
          },
          gender: {
            required: 'Campo obligatorio'
          }
        },
        rules: {
          rol_id: {
            required: true
          },
          name: {
            required: true
          },
          email: {
            required: true
          },
          password: {
            required: true
          },
          gender: {
            required: true
          }
        },

        errorPlacement: function (error, element) {
          // render error placement for each input type
          var cont = $(element).parent('.validate')
          console.log(error)
          if (cont) {
            cont.after(error)
          } else {
            element.after(error)
          }
        },

        highlight: function (element) {
          // entradas de error de hightlight
          $(element)
            .closest('.form-group')
            .removeClass('has-success')
            .addClass('has-error') // set error class to the control group
        },

        unhighlight: function (element) {
          // revertir el cambio realizado por hightlight
          $(element)
            .closest('.form-group')
            .removeClass('has-error')
            .addClass('has-success') // set error class to the control group
        },
        //
        success: function (label) {
          label
            .closest('.form-group')
            .removeClass('has-error')
            .addClass('has-success') // set success class to the control group
        }
      })

      // SweetAlert Eliminar
      var $delete = $('.btn-delete')
      var $table = $('.table-model')
      $delete.click(function (e) {
        var $data = $(this).data()
        e.preventDefault()
        swal(
          {
            title: '¿Esta seguro de Eliminar?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info btn-fill',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar',
            cancelButtonClass: 'btn btn-danger btn-fill',
            closeOnConfirm: false,
            allowOutsideClick: false
          },
          function () {
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              type: 'POST',
              dataType: 'json',
              url: $data.url,
              async: false,
              data: { id: $data.id },
              success: function (data) {
                if (data.status == 200) {
                  swal('¡Eliminado!', 'Fue eliminado con exito', 'success')
                  $('tr[data-id=' + $data.id + ']', $table).remove()
                  setTimeout(function () {
                    window.location.reload()
                  }, 2000)
                } else swal('Error al eliminar', data.message, 'error')
              }
            })
          }
        )
      })
    }
  }
})()

var odont = (function () {
  return {
    init: function () {
      // Validaciones
      // datos de prueba
      var color = 1

      var datos = [
        { diente: 1, cara: 1, estado: 8, tipo: 1 },
        { diente: 2, cara: 2, estado: 2, tipo: 1 },
        { diente: 2, cara: 5, estado: 1, tipo: 1 },
        { diente: 3, cara: 2, estado: 6, tipo: 1 },
        { diente: 4, cara: 2, estado: 4, tipo: 1 }
      ]

      //var color_desabled = "rgb(171, 183, 183)";
      var color_desabled = "rgb(067, 075, 077)";
     //var color_enabled = "rgba(232, 232, 232, 1)";
      var color_enabled = "rgba(255, 255, 255)";

      desabledAll();

      $(document).ready(function () {      
        //Lesion caries dental
        var array_btn_lesion_caries_dental = ['18','17','16','15','14','13','12','11','21','22','23','24','25','26','27','28',
        '55','54','53','52','51','61','62','63','64','65',
        '85','84','83','82','81','71','72','73','74','75',
        '48','47','46','45','44','43','42','41','31','32','33','34','35','36','37','38'];
        listenToButton('btn_lesion_caries_dental', array_btn_lesion_caries_dental)

        //DDE
        var array_btn_dde = ['15','14','16'];
        listenToButton('btn_DDE', array_btn_dde)

        //Sellantes
        var array_btn_sellantes = ['15','16','17'];
        listenToButton('btn_Sellantes', array_btn_sellantes)

        //Fractura
        var array_btn_fractura = ['13','12','11'];
        listenToButton('btn_Fracturas', array_btn_fractura)

        //Fosas y fisuras profundas
        var array_btn_fosas_fisuras_profundas = ['26','27','28'];
        listenToButton('btn_Fosas_Fisuras_Profundas', array_btn_fosas_fisuras_profundas)

        //Pieza Dentaria Ausente
        var array_btn_pieza_dentaria_ausente = ['23','24','25'];
        listenToButton('btn_Ausente', array_btn_pieza_dentaria_ausente)

        //Pieza Dentaria en Erupcion
        var array_btn_dentaria_en_erupcion = ['23','24','25','31','32','33'];
        listenToButton('btn_Erupcion', array_btn_dentaria_en_erupcion)

        //Restauracion definitiva
        var array_btn_restauracion_definitiva = ['17','16','15'];
        listenToButton('btn_Restauracion_Definitiva', array_btn_restauracion_definitiva)

        //Restauracion Temporal
        var array_btn_restauracion_temporal = ['34','35','36'];
        listenToButton('btn_Restauracion_Temporal', array_btn_restauracion_temporal)

        //Edentulo Total
        var array_btn_edentulo_total = ['18','17','16','15','14','13','12','11','21','22','23','24','25','26','27','28'];
        listenToButton('btn_Edentulo_Total', array_btn_edentulo_total)

        //Pieza Dentaria supernumerica
        var array_btn_pieza_dentaria_supernumeria = ['12','11','21','22'];
        listenToButton('btn_Supernumeraria', array_btn_pieza_dentaria_supernumeria)

        //Pieza dentaria extruida
        var array_btn_pieza_dentaria_extruida = ['12','11','21','22','18','17','16'];
        listenToButton('btn_Extruida', array_btn_pieza_dentaria_extruida)

        //Pieza Dentaria Intruida
        var array_btn_pieza_dentaria_intruida = ['12','11','21','22','18','17','16'];
        listenToButton('btn_Intruida', array_btn_pieza_dentaria_intruida)

        //Diastema
        var array_btn_diastema = ['12','11','21','22'];
        listenToButton('btn_Diastema', array_btn_diastema)

        //Giroversion
        var array_btn_giroversion= ['17','16','15'];
        listenToButton('btn_Girobersion', array_btn_giroversion)

        //Posicion Dentaria
        var array_btn_posicion_dentaria = ['18','17','16'];
        listenToButton('btn_Posicion_Dentaria', array_btn_posicion_dentaria)

        //Pieza Dentaria en clavicula
        var array_btn_pieza_dentaria_clavicula = ['11'];
        listenToButton('btn_Clavija', array_btn_pieza_dentaria_clavicula)

        //PD Ectopica
        var array_btn_pieza_dentaria_ectopica = ['12','11','21','22'];
        listenToButton('btn_Ectopica', array_btn_pieza_dentaria_ectopica)

        //Macrodoncia
        var array_btn_macrodoncia = ['24','25','26','27'];
        listenToButton('btn_Macrodoncia', array_btn_macrodoncia)

        //Microdoncia
        var array_btn_microdonia = ['24','25','26','27'];
        listenToButton('btn_Microdoncia', array_btn_microdonia)

        //Fusión
        var array_btn_fusion = ['12','11','21','22'];
        listenToButton('btn_Fusion', array_btn_fusion)

        //Geminacion
        var array_btn_geminacion = ['12','11'];
        listenToButton('btn_Geminacion', array_btn_geminacion)

        //impactacion
        var array_btn_impactacion = ['35','36','37'];
        listenToButton('btn_Impactacion', array_btn_impactacion)

        //Desgastada
        var array_btn_desgastada = ['12','11','21','22'];
        listenToButton('btn_Desgastada', array_btn_desgastada)

        //Remanente radicular 
        var array_btn_remanente_radicular = ['37','36','35'];
        listenToButton('btn_Radicular', array_btn_remanente_radicular)

        //movilidad patologica
        var array_btn_movilidad_patologica = ['34','35','36'];
        listenToButton('btn_Movilidad', array_btn_movilidad_patologica)

        //Corona Temporal
        var array_btn_corona_temporal = ['47','46','45'];
        listenToButton('btn_Corona_Temporal', array_btn_corona_temporal)

        //Corona
        var array_btn_corona = ['47','46','45','25','26','27'];
        listenToButton('btn_Corona', array_btn_corona)

        //Espigo-Muñon
        var array_btn_espigo_muñon = ['25','26','27'];
        listenToButton('btn_Espigo', array_btn_espigo_muñon)

        //Implante dental
        var array_btn_implante_dental = ['25','26','27'];
        listenToButton('btn_Implante_Dental', array_btn_implante_dental)

        //Aparato ortodontico fijo
        var array_btn_aparato_ortodontico_fijo = ['17','16','15','14','13','12','11','21','22','23','24','25','26','27'];
        listenToButton('btn_Aparato_Fijo', array_btn_aparato_ortodontico_fijo)

        //Aparato ortodontico removible
        var array_btn_aparato_ortodontico_remivible = ['17','16','15','14','13','12','11','21','22','23','24','25','26','27'];
        listenToButton('btn_Aparato_Removible', array_btn_aparato_ortodontico_remivible)

        //Protesis fija
        var array_btn_protesis_fija = ['17','16','15','14','13','12','11','21','22','23','24','25','26','27'];
        listenToButton('btn_Protesis_Fija', array_btn_protesis_fija)

        $('.color').click(function () {
          color = $(this).attr('value')
        })
      })


      //nos servira para poner ala escucha y poder hacer el pintado
      function listenToPaint(Son_Id, Fhater_Id) {
        $('#' + Son_Id).on('click', function () {
          var valor = document.getElementById(Fhater_Id).style.getPropertyValue("fill");
          if (valor != color_desabled) {
            pintar(color, this)
          }
        })
      }

      //hacemos la escucha del boton que se va a seleccionar
      function listenToButton(Btn_Id, array) {
        $('#' + Btn_Id).on('click', function () {
          desabledAll();
          array.forEach(function (Number_To_Active, indice, array) {
            enabledListed(Number_To_Active);
            enabledSpecific(Number_To_Active);
          });          
        })
      }

      //poner ala escucha un numero espesifico del diente
      function enabledListed(N) {

        listenToPaint("Rectangle_" + N, "N" + N);

        listenToPaint("Triangle_" + N + "_1", "N" + N);
        listenToPaint("Triangle_" + N + "_2", "N" + N);
        listenToPaint("Triangle_" + N + "_3", "N" + N);

        listenToPaint("Polygon_" + N + "_1", "N" + N);
        listenToPaint("Polygon_" + N + "_2", "N" + N);
        listenToPaint("Polygon_" + N + "_3", "N" + N);
        listenToPaint("Polygon_" + N + "_4", "N" + N);
        listenToPaint("Polygon_" + N + "_5", "N" + N);

        listenToPaint("Rectangle_Inside_" + N + "_1", "N" + N);
        listenToPaint("Rectangle_Inside_" + N + "_2", "N" + N);
        listenToPaint("Rectangle_Inside_" + N + "_3", "N" + N);
        listenToPaint("Rectangle_Inside_" + N + "_4", "N" + N);
        listenToPaint("Rectangle_Inside_" + N + "_5", "N" + N);
        listenToPaint("Rectangle_Inside_" + N + "_6", "N" + N);
      }

      //hacemos el pintado en la casilla seleccionada
      function pintar(color, objeto) {
        if (color == 1) {
          $(objeto).attr({
            class: 'marcadoRojo marcado',
            estado: color
          })
        } else if (color == 2) {
          $(objeto).attr({
            class: 'marcadoAmarillo marcado',
            estado: color
          })
        } else if (color == 3) {
          $(objeto).attr({
            class: 'marcadoVerde marcado',
            estado: color
          })
        } else if (color == 4) {
          $(objeto).attr({
            class: 'marcadoAzul marcado',
            estado: color
          })
        } else if (color == 5) {
          $(objeto).attr({
            class: 'diente',
            estado: color
          })
        }
      }

      //inabilitadmos todos los componentes y hacemos el pintado de inablitado
      function desabledAll() {
        document.getElementById('N18').style.fill = color_desabled;
        document.getElementById('N17').style.fill = color_desabled;
        document.getElementById('N16').style.fill = color_desabled;
        document.getElementById('N15').style.fill = color_desabled;
        document.getElementById('N14').style.fill = color_desabled;
        document.getElementById('N13').style.fill = color_desabled;
        document.getElementById('N12').style.fill = color_desabled;
        document.getElementById('N11').style.fill = color_desabled;
        document.getElementById('N21').style.fill = color_desabled;
        document.getElementById('N22').style.fill = color_desabled;
        document.getElementById('N23').style.fill = color_desabled;
        document.getElementById('N24').style.fill = color_desabled;
        document.getElementById('N25').style.fill = color_desabled;
        document.getElementById('N26').style.fill = color_desabled;
        document.getElementById('N27').style.fill = color_desabled;
        document.getElementById('N28').style.fill = color_desabled;

        document.getElementById('N55').style.fill = color_desabled;
        document.getElementById('N54').style.fill = color_desabled;
        document.getElementById('N53').style.fill = color_desabled;
        document.getElementById('N52').style.fill = color_desabled;
        document.getElementById('N51').style.fill = color_desabled;
        document.getElementById('N61').style.fill = color_desabled;
        document.getElementById('N62').style.fill = color_desabled;
        document.getElementById('N63').style.fill = color_desabled;
        document.getElementById('N64').style.fill = color_desabled;
        document.getElementById('N65').style.fill = color_desabled;

        document.getElementById('N85').style.fill = color_desabled;
        document.getElementById('N84').style.fill = color_desabled;
        document.getElementById('N83').style.fill = color_desabled;
        document.getElementById('N82').style.fill = color_desabled;
        document.getElementById('N81').style.fill = color_desabled;
        document.getElementById('N71').style.fill = color_desabled;
        document.getElementById('N72').style.fill = color_desabled;
        document.getElementById('N73').style.fill = color_desabled;
        document.getElementById('N74').style.fill = color_desabled;
        document.getElementById('N75').style.fill = color_desabled;

        document.getElementById('N48').style.fill = color_desabled;
        document.getElementById('N47').style.fill = color_desabled;
        document.getElementById('N46').style.fill = color_desabled;
        document.getElementById('N45').style.fill = color_desabled;
        document.getElementById('N44').style.fill = color_desabled;
        document.getElementById('N43').style.fill = color_desabled;
        document.getElementById('N42').style.fill = color_desabled;
        document.getElementById('N41').style.fill = color_desabled;
        document.getElementById('N31').style.fill = color_desabled;
        document.getElementById('N32').style.fill = color_desabled;
        document.getElementById('N33').style.fill = color_desabled;
        document.getElementById('N34').style.fill = color_desabled;
        document.getElementById('N35').style.fill = color_desabled;
        document.getElementById('N36').style.fill = color_desabled;
        document.getElementById('N37').style.fill = color_desabled;
        document.getElementById('N38').style.fill = color_desabled;
        

      }

      //habilitamos un diente en espesifico para que se pueda pintar 
      function enabledSpecific(Number_To_Enabled) {     
        document.getElementById('N' + Number_To_Enabled).style.fill = color_enabled;
      }
    }
  }
})()
var index = (function () {
  return {
    init: function (appointments, arrayYears) {
      var arrayAppointments = appointments
      var titleYears = []
      var dataAtended = []
      var dataAbsent = []

      var e = new Date()
      var year = e.getFullYear()
      var r = {}

      arrayYears.forEach(function (element) {
        titleYears.push(element.anio)
        dataAtended.push(0)
        dataAbsent.push(0)
      })

      arrayAppointments.forEach(function (elementAppointments) {
        titleYears.forEach(function (elementYear) {
          if (elementAppointments.state == 'attended') {
            if (elementYear == elementAppointments.anio) {
              var valor = getNumberAnio(elementAppointments.anio)
              dataAtended[valor] = parseInt(dataAtended[valor]) + 1
            }
          } else if (elementAppointments.state == 'absent') {
            if (elementYear == elementAppointments.anio) {
              var valor = getNumberAnio(elementAppointments.anio)
              dataAbsent[valor] = parseInt(dataAbsent[valor]) + 1
            }
          }
        })
      });

      new Chart(document.getElementById('bar-chart'), {
        type: 'bar',
        data: {
          labels: titleYears,
          datasets: [
            {
              label: 'Atendidos',
              backgroundColor: '#8e5ea2',
              data: dataAtended
            },
            {
              label: 'Ausentes',
              backgroundColor: '#8e5ea5',
              data: dataAbsent
            }
          ]
        },
        options: {
          title: {
            display: true,
            text: 'REPORTE DE CITAS POR AÑO'
          }
        }
      })

      function getNumberAnio(anio) {
        var contador = 0
        var valor = -1
        titleYears.forEach(function (element) {
          if (element == anio) {
            valor = contador
          }
          contador = parseInt(contador) + 1
        })
        return valor
      }
    }
  }
})()


var specialty = (function () {
  return {
    init: function () {
      // Validaciones
      var form1 = $('#specialty_form')
      form1.validate({
        errorElement: 'span', // default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: '', // validate all fields including form hidden inpu
        messages: {
          name: {
            required: 'Campo obligatorio'
          }
        },
        rules: {
          name: {
            required: true
          }
        },

        errorPlacement: function (error, element) {
          // render error placement for each input type
          var cont = $(element).parent('.validate')

          if (cont) {
            cont.after(error)
          } else {
            element.after(error)
          }
        },

        highlight: function (element) {
          // entradas de error de hightlight
          $(element)
            .closest('.form-group')
            .removeClass('has-success')
            .addClass('has-error') // set error class to the control group
        },

        unhighlight: function (element) {
          // revertir el cambio realizado por hightlight
          $(element)
            .closest('.form-group')
            .removeClass('has-error')
            .addClass('has-success') // set error class to the control group
        },
        //
        success: function (label) {
          label
            .closest('.form-group')
            .removeClass('has-error')
            .addClass('has-success') // set success class to the control group
        }
      })

      // SweetAlert Eliminar
      var $delete = $('.btn-delete')
      var $table = $('.table-model')
      $delete.click(function (e) {
        var $data = $(this).data()
        e.preventDefault()
        swal(
          {
            title: '¿Esta seguro de Eliminar?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info btn-fill',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar',
            cancelButtonClass: 'btn btn-danger btn-fill',
            closeOnConfirm: false,
            allowOutsideClick: false
          },
          function () {
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              type: 'POST',
              dataType: 'json',
              url: $data.url,
              async: false,
              data: { id: $data.id },
              success: function (data) {
                if (data.status == 200) {
                  swal('¡Eliminado!', 'Fue eliminado con exito', 'success')
                  $('tr[data-id=' + $data.id + ']', $table).remove()
                  setTimeout(function () {
                    window.location.reload()
                  }, 2000)
                } else swal('Error al eliminar', data.message, 'error')
              }
            })
          }
        )
      })
    }
  }
})()

function fn_croppic($id, $url, $url_delete) {
  var $objImage = $('#' + $id)
  var $data = $objImage.data()
  var croppicOptions = {
    cropUrl: $url,
    processInline: true,
    modal: true,
    cropData: {
      id: $data.model
    },
    imgEyecandyOpacity: 0.4,
    loaderHtml:
      '<div class="loader bubblingG"><span id="bubblingG_1">' +
      '</span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>',
    onAfterImgCrop: function (data) {
      if (data.state == 200) {
        $objImage.data('id', data.id)
      }
    },
    onBeforeRemoveCroppedImg: function () {
      delete_crop($id, this, $url_delete)
      $objImage
        .find('.cropControlsUpload')
        .find('.cropControlRemoveCroppedImage')
        .remove()
    },
    onError: function (errormessage) {
      console.log('onError:' + errormessage)
    }
  }
  var cropContainer = new Croppic($id, croppicOptions)
  if ($data.image) {
    $objImage
      .find('.cropControlsUpload')
      .append('<i class="cropControlRemoveCroppedImage"></i>')
    var btn_remove = $('#' + $id)
      .find('.cropControlsUpload')
      .find('.cropControlRemoveCroppedImage')
    btn_remove.on('click', function () {
      delete_crop($id, cropContainer, $url_delete)
    })
  }
}

function delete_crop(imgCrop, objCrop, $url_delete) {
  var $objImg = $('#' + imgCrop)
  var data = { id: $objImg.data('id') }
  $.ajax({
    type: 'POST',
    dataType: 'json',
    url: $url_delete,
    async: false,
    data: data,
    success: function (data) {
      if (!data.error) {
        $objImg.find('.croppedImg').remove()
        $objImg
          .find('.cropControlsUpload')
          .find('.cropControlRemoveCroppedImage')
          .remove()
        $objImg.attr('data-imagen', '')
        objCrop.reset()
      }
    },
    error: function () {
      console.log('error')
    }
  })
}

var package_image = (function () {
  return {
    init: function () {
      fn_croppic(
        'cropImgUser',
        '/admin/package/image/banner',
        '/admin/package/image/delete/banner'
      )
    }
  }
})()

function buscarSelect(estado, idSelecctor) {

  var select = document.getElementById(idSelecctor)
  for (var i = 0; i < select.length; i++) {
    if (select.options[i].text == estado) {
      select.selectedIndex = i
    }
  }
}

function convertDateFormat(string) {
  var info = string.split('/')
  return info[2] + '-' + info[1] + '-' + info[0]
}

//DASHBOAR
var dashboard = (function () {
  return {
    init: function () {
      var e = new Date,
        year = e.getFullYear(),
        r = {};
      console.log(year)
      buscarSelect(year, 'cbo_reportAnio');

    }
  }

})()


//crud Appointments
var appointment_create = (function () {
  return {
    init: function (start_Date, doctors) {
      var form1 = $('#appointments_form')
      var data = form1.data();
      $('#doctor_id').select2();
      $('#state').css('display', 'none');
      $('#lbl_Estate').css('display', 'none');

      if (start_Date != null) {
        $('#date-start').val(start_Date);
        var fecha = new Date(start_Date);
        $('#date-start').bootstrapMaterialDatePicker({
          format: 'MM/DD/YYYY HH:mm',
          minDate: fecha,
        })
        $('#date-end').bootstrapMaterialDatePicker({
          format: 'MM/DD/YYYY HH:mm',
          minDate: fecha,
        });
      }
      enableAll(true);

      var funcionStartDate = function () {
        var start_Date = document.getElementById('date-start').value;
        var doctor_id = document.getElementById('doctor_id').value;
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
              'content'
            )
          },
          type: 'GET',
          dataType: 'json',
          url: data.doc,
          async: false,
          data: {
            start_Date: start_Date,
            doctor_id: doctor_id
          },
          success: function (resultado) {
            if (!resultado) {
              //error 
              var ele = $("#date-start");
              ele
                .closest('.form-group')
                .removeClass('has-success')
                .addClass('has-error')
              enableDateEnd(true)
            } else {
              var ele = $("#date-start");
              ele
                .closest('.form-group')
                .removeClass('has-error')
                .addClass('has-success')

              enableDateEnd(false)
            }
          }
        })
      }

      /* Verificar y buscar los stanbyTime de los doctores que se registro anteriormente*/
      $("select[id=doctor_id]").change(function () {
        var id = $('select[id=doctor_id]').val();
        $('#standbyTime').val(0);
        doctors.forEach(function (element) {
          if (element.id == id) {
            $('#standbyTime').val(element.standbyTime);
          }
        });

        if (id == 0) {
          enableDescription(true);
        } else {
          enableDescription(false);
          setInterval(funcionStartDate, 10000);
        }
      });





      function enableAll(state) {
        $('#id_description').prop("disabled", state);
        $('#btn_verifyStartDate').prop("disabled", state);
        $('#date-start').prop("disabled", state);
        $('#date-end').prop("disabled", state);
        $('#id_price').prop("disabled", state);
        $('#standbyTime').prop("disabled", state);
        $('#btn_guardar').prop("disabled", state);
      }
      function enableDescription(state) {
        $('#id_description').prop("disabled", state);
        $('#btn_verifyStartDate').prop("disabled", state);
        $('#date-start').prop("disabled", state);

      }
      function enableDateEnd(state) {
        $('#date-end').prop("disabled", state);
        $('#id_price').prop("disabled", state);
        $('#standbyTime').prop("disabled", state);
        $('#btn_guardar').prop("disabled", state);
      }
      function enablePrice(state) {
        $('#id_price').prop("disabled", state);
        $('#standbyTime').prop("disabled", state);
        $('#btn_guardar').prop("disabled", state);
      }
    }
  }

})()
var appointment_edit = (function () {
  return {
    init: function (doctors) {

      $('#doctor_id').select2();
      /*  $('#state').css('display', 'none'); */
      /*  $('#lbl_Estate').css('display', 'none'); */

      //obtenemos la fecha pero sin los minutos no se puede usar carbon en js 
      var date_start = $('#date-start').val().substring(0, 10);
      var date_end = $('#date-end').val().substring(0, 10);

      $('#date-start').bootstrapMaterialDatePicker({
        format: 'MM/DD/YYYY HH:mm',
        minDate: new Date(date_start),
      })

      $('#date-end').bootstrapMaterialDatePicker({
        format: 'MM/DD/YYYY HH:mm',
        minDate: new Date(date_end),
      });

      /* Verificar y buscar los stanbyTime de los doctores que se registro anteriormente*/
      $("select[id=doctor_id]").change(function () {
        var id = $('select[id=doctor_id]').val();
        $('#standbyTime').val(0);
        doctors.forEach(function (element) {
          if (element.id == id) {
            $('#standbyTime').val(element.standbyTime);
          }
        });

      });
    }
  }

})()
var appointment_form = (function () {
  return {
    init: function () {
      var form1 = $('#appointments_form')
      var data = form1.data();
      var _token = $("input[name=_token]").val();
      form1.validate({
        errorElement: 'span', // default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: '', // validate all fields including form hidden inpu
        messages: {
          description: {
            required: 'Campo obligatorio'
          },
          start_Date: {
            required: 'Campo obligatorio',
            remote: 'Fecha ya registrada '
          },
          end_Date: {
            required: 'Campo obligatorio'
          },
          patient_id: {
            required: 'Campo obligatorio'
          },
          doctor_id: {
            required: 'Campo obligatorio'
          }
        },
        rules: {
          description: {
            required: true
          },
          start_Date: {
            required: true,

          },
          end_Date: {
            required: true
          },
          patient_id: {
            required: true
          },
          doctor_id: {
            required: true
          }
        },

        errorPlacement: function (error, element) {
          // render error placement for each input type
          var cont = $(element).parent('.validate')
          if (cont) {
            cont.after(error)
          } else {
            element.after(error)
          }
        },

        highlight: function (element) {
          // entradas de error de hightlight
          $(element)
            .closest('.form-group')
            .removeClass('has-success')
            .addClass('has-error') // set error class to the control group
        },

        unhighlight: function (element) {
          // revertir el cambio realizado por hightlight
          $(element)
            .closest('.form-group')
            .removeClass('has-error')
            .addClass('has-success') // set error class to the control group
        },
        //
        success: function (label) {
          label
            .closest('.form-group')
            .removeClass('has-error')
            .addClass('has-success') // set success class to the control group
        }
      });




    }
  }

})()
var appointment_delete = (function () {
  return {
    init: function () {
      var $delete = $('.btn-delete')
      var $table = $('.table-model')
      $delete.click(function (e) {
        var $data = $(this).data()
        e.preventDefault()
        swal(
          {
            title: '¿Esta seguro de Eliminar?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info btn-fill',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar',
            cancelButtonClass: 'btn btn-danger btn-fill',
            closeOnConfirm: false,
            allowOutsideClick: false
          },
          function () {
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              type: 'POST',
              dataType: 'json',
              url: $data.url,
              async: false,
              data: { id: $data.id },
              success: function (data) {
                if (data.status == 200) {
                  swal('¡Eliminado!', 'Fue eliminado con exito', 'success')
                  $('tr[data-id=' + $data.id + ']', $table).remove()
                  setTimeout(function () {
                    window.location.reload()
                  }, 2000)
                } else swal('Error al eliminar', data.message, 'error')
              }
            })
          }
        )
      })

    }
  }

})()
var appointmentCalendar_index = (function () {
  return {
    init: function (doctors, doctor_default, state) {



      //llenado id seleccionados
      $("#lbl_doctor_select").html(doctor_default);
      $("#lbl_state_select").html(state);



      //paint botton
      switch (state) {
        case "All":
          document.getElementById('state_all').style.backgroundColor = "#000000";
          document.getElementById('state_Pending').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Absent').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Attended').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Cancel').style.backgroundColor = "#D3D3D3";
          break;
        case "pending":
          document.getElementById('state_all').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Pending').style.backgroundColor = "#FFB236";
          document.getElementById('state_Absent').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Attended').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Cancel').style.backgroundColor = "#D3D3D3";
          break;
        case "absent":
          document.getElementById('state_all').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Pending').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Absent').style.backgroundColor = "#fc0703";
          document.getElementById('state_Attended').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Cancel').style.backgroundColor = "#D3D3D3";
          break;
        case "attended":
          document.getElementById('state_all').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Pending').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Absent').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Attended').style.backgroundColor = "#18ce0f";
          document.getElementById('state_Cancel').style.backgroundColor = "#D3D3D3";
          break;
        case "cancel":
          document.getElementById('state_all').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Pending').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Absent').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Attended').style.backgroundColor = "#D3D3D3";
          document.getElementById('state_Cancel').style.backgroundColor = "#FF3636";
          break;
        default:
          document.getElementById('state_all').style.backgroundColor = "#D3D3D3";
          break;
      }

      $("#" + doctor_default).css("opacity", "100");

      //click eventos
      $('#state_all').click(function ($) {
        search_appointments_for_state('All');
      });
      $('#state_Pending').click(function ($) {
        search_appointments_for_state('pending');
      });
      $('#state_Attended').click(function ($) {
        search_appointments_for_state('attended');
      });
      $('#state_Absent').click(function ($) {
        search_appointments_for_state('absent');
      });
      $('#state_Cancel').click(function ($) {
        search_appointments_for_state('cancel');
      });


      function search_appointments_for_state(state) {
        var slug_doctor = $("#lbl_doctor_select").html();
        var join = slug_doctor + "." + state;
        var url = null;
        if (slug_doctor == 'All' && state == "All") {
          var btn_AppointmentIndex = document.getElementById('btn_AppointmentIndex');
          url = btn_AppointmentIndex.getAttribute('href')
        } else {
          var botonSearchAppointmentsForDoctor = document.getElementById('botonSearchAppointmentsForDoctor');
          url = botonSearchAppointmentsForDoctor.getAttribute('href')
          url = url.substring(0, url.length - 1)
          url = url + '' + join;
        }

        document.getElementById('botonSearchAppointmentsForDoctor').href = url;
        document.getElementById('botonSearchAppointmentsForDoctor').click();


      }

      //funcionalidad del calindario




    }
  }

})()


//crud patients
var patient_create = (function () {
  return {
    init: function () {

      activeInput(true);
      //select cambios de tipo documento
      $("select[id=type_document]").change(function () {
        var id = $('select[id=type_document]').val();
        var num_document = document.getElementById('num_document')
        var last_name = document.getElementById('last_name')
        var name = document.getElementById('name')

        if (id == 0) {
          activeInput(true);
        } else if (id == 1) {
          activeInput(false);
          num_document.setAttribute('maxlength', 8)
          num_document.value = ''
          last_name.value = ''
          name.value = ''
          num_document.setAttribute('placeHolder', 'Ingrese DNI')
          buscarSelect('Seleccionado', 'gender')
        } else {
          activeInput(false);
          num_document.setAttribute('maxlength', 12)
          num_document.value = ''
          last_name.value = ''
          name.value = ''
          num_document.setAttribute('placeHolder', 'Ingrese Pasaporte')
          buscarSelect('Seleccionado', 'gender')
        }
      });



      var numDocument = $('#num_document')
      numDocument.keypress(function (event) {
        var keycode = event.keyCode ? event.keyCode : event.which
        if (keycode == '13') {
          // verificamos si es DNI o Pasaporte
          if (type_document.value == '1') {
            var num_document = document.getElementById('num_document').value
            var btn_SearchDniPatient = document.getElementById('btn_SearchDniPatient')
            var btn_SearchDniPatient = btn_SearchDniPatient.getAttribute('href')
            if (num_document.length == 8) {
              $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                    'content'
                  )
                },
                type: 'GET',
                dataType: 'json',
                url: btn_SearchDniPatient,
                async: false,
                data: { dni: num_document },
                success: function (resultado) {
                  if (resultado.state == 'Correcto') {
                    document.getElementById('name').value = ucwords(resultado.nombre.toLowerCase());
                    document.getElementById('last_name').value =
                      ucwords(resultado.paterno.toLowerCase() + ' ' + resultado.materno.toLowerCase());
                    document.getElementById('last_name').value =
                      ucwords(resultado.paterno.toLowerCase() + ' ' + resultado.materno.toLowerCase());
                    document.getElementById(
                      'birth_date_patients'
                    ).value = convertDateFormat(resultado.nacimiento);
                    buscarSelect(resultado.sexo, 'gender')
                  } else {
                    swal(
                      'DNI incorrecto',
                      'Por favor verifique el dni ingresado y vuelva a intentar',
                      'error'
                    )
                    document.getElementById('num_document').value = ''
                    document.getElementById('last_name').value = ''
                    document.getElementById('name').value = ''
                    buscarSelect('Seleccionado', 'gender')
                  }
                }
              })
            }
          }
        }
      })

      $("#btn_buscar").click(function () {
        document.getElementById('img_loading_patient').style.display = "inline";
        var funcion = function () {
          var num_document = document.getElementById('num_document').value
          var btn_SearchDniPatient = document.getElementById('btn_SearchDniPatient')
          var btn_SearchDniPatient = btn_SearchDniPatient.getAttribute('href')
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                'content'
              )
            },
            type: 'GET',
            dataType: 'json',
            url: btn_SearchDniPatient,
            async: false,
            data: { dni: num_document },
            success: function (resultado) {
              document.getElementById('img_loading_patient').style.display = "none";
              if (resultado.state == 'Correcto') {
                document.getElementById('name').value = ucwords(resultado.nombre.toLowerCase());
                document.getElementById('last_name').value =
                  ucwords(resultado.paterno.toLowerCase() + ' ' + resultado.materno.toLowerCase());
                document.getElementById('last_name').value =
                  ucwords(resultado.paterno.toLowerCase() + ' ' + resultado.materno.toLowerCase());
                document.getElementById(
                  'birth_date_patients'
                ).value = convertDateFormat(resultado.nacimiento);
                buscarSelect(resultado.sexo, 'gender')
              } else {
                swal(
                  'DNI incorrecto',
                  'Por favor verifique el dni ingresado y vuelva a intentar',
                  'error'
                )
                document.getElementById('num_document').value = ''
                document.getElementById('last_name').value = ''
                document.getElementById('name').value = ''
                buscarSelect('Seleccionado', 'gender')
              }
            }
          })
        }

        setTimeout(funcion, 2000);
      });
      function ucwords(str) {
        return (str + '')
          .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
            return $1.toUpperCase();
          });
      }



      //function para activar las casillas 
      function activeInput(state) {
        $('#num_document').prop("disabled", state);
        $('#name').prop("disabled", state);
        $('#last_name').prop("disabled", state);
        $('#gender').prop("disabled", state);
        $('#address').prop("disabled", state);
        $('#email').prop("disabled", state);
        $('#civil_status').prop("disabled", state);
        $('#mobile').prop("disabled", state);
        $('#phone').prop("disabled", state);
        $('#occupation').prop("disabled", state);
        $('#workplace').prop("disabled", state);
        $('#birth_date_patients').prop("disabled", state);
        $('#btn-add').prop("disabled", state);
        $('#btn-cancel').prop("disabled", state);
        if (state == false) {
          $('#btn_buscar').css("display", "inline");
        } else {
          $('#btn_buscar').css("display", "none");
        }
      }
    }
  }
})()
var patient_Edit = (function () {
  return {
    init: function (appointments) {

      activeInput(false);
      //recorrer el array de appointments
      var totalPendientes = 0;
      var totalAusentes = 0;
      var totalCancelados = 0;
      var totalAtendidos = 0;
      appointments.forEach(function (element) {
        if (element.state == 'Atendido') {
          totalAtendidos = parseInt(totalAtendidos) + 1;
        } else if (element.state == 'Pendiente') {
          totalPendientes = parseInt(totalPendientes) + 1;
        } else if (element.state == 'Ausente') {
          totalAusentes = parseInt(totalAusentes) + 1;
        } else if (element.state == 'Cancelado') {
          totalCancelados = parseInt(totalCancelados) + 1;
        }
      })
      //llenado de datos en los totales de cada uno 

      $('#totalCitasPendientes').html(totalPendientes);
      $('#totalCitasAtendidos').html(totalAtendidos);
      $('#totalCitasCancelados').html(totalCancelados);
      $('#totalCitasAusentes').html(totalAusentes);

      //select cambios de tipo documento
      $("select[id=type_document]").change(function () {
        var id = $('select[id=type_document]').val();
        var num_document = document.getElementById('num_document')
        var last_name = document.getElementById('last_name')
        var name = document.getElementById('name')

        if (id == 0) {
          activeInput(true);
        } else if (id == 1) {
          activeInput(false);
          num_document.setAttribute('maxlength', 8)
          num_document.value = ''
          last_name.value = ''
          name.value = ''
          num_document.setAttribute('placeHolder', 'Ingrese DNI')
          buscarSelect('Seleccionado', 'gender')
        } else {
          activeInput(false);
          num_document.setAttribute('maxlength', 12)
          num_document.value = ''
          last_name.value = ''
          name.value = ''
          num_document.setAttribute('placeHolder', 'Ingrese Pasaporte')
          buscarSelect('Seleccionado', 'gender')
        }
      });

      var numDocument = $('#num_document')
      numDocument.keypress(function (event) {
        var keycode = event.keyCode ? event.keyCode : event.which
        if (keycode == '13') {
          // verificamos si es DNI o Pasaporte
          if (type_document.value == '1') {
            var num_document = document.getElementById('num_document').value
            var btn_SearchDniPatient = document.getElementById('btn_SearchDniPatient')
            var btn_SearchDniPatient = btn_SearchDniPatient.getAttribute('href')
            if (num_document.length == 8) {
              $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                    'content'
                  )
                },
                type: 'GET',
                dataType: 'json',
                url: btn_SearchDniPatient,
                async: false,
                data: { dni: num_document },
                success: function (resultado) {
                  if (resultado.state == 'Correcto') {
                    document.getElementById('name').value = ucwords(resultado.nombre.toLowerCase());
                    document.getElementById('last_name').value =
                      ucwords(resultado.paterno.toLowerCase() + ' ' + resultado.materno.toLowerCase());
                    document.getElementById('last_name').value =
                      ucwords(resultado.paterno.toLowerCase() + ' ' + resultado.materno.toLowerCase());
                    document.getElementById('birth_date_patients').value = convertDateFormat(resultado.nacimiento);
                    buscarSelect(resultado.sexo, 'gender')
                  } else {
                    swal(
                      'DNI incorrecto',
                      'Por favor verifique el dni ingresado y vuelva a intentar',
                      'error'
                    )
                    document.getElementById('num_document').value = ''
                    document.getElementById('last_name').value = ''
                    document.getElementById('name').value = ''
                    buscarSelect('Seleccionado', 'gender')
                  }
                }
              })
            }
          }
        }
      })
      function ucwords(str) {
        return (str + '')
          .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
            return $1.toUpperCase();
          });
      }

      //function para activar las casillas 
      function activeInput(state) {
        $('#num_document').prop("disabled", state);
        $('#name').prop("disabled", state);
        $('#last_name').prop("disabled", state);
        $('#gender').prop("disabled", state);
        $('#address').prop("disabled", state);
        $('#email').prop("disabled", state);
        $('#civil_status').prop("disabled", state);
        $('#mobile').prop("disabled", state);
        $('#phone').prop("disabled", state);
        $('#occupation').prop("disabled", state);
        $('#workplace').prop("disabled", state);
        $('#birth_date_patients').prop("disabled", state);
        $('#btn-add').prop("disabled", state);
        $('#btn-cancel').prop("disabled", state);

      }
    }
  }
})()
var patient_form = (function () {
  return {
    init: function () {
      // Validaciones
      var form1 = $('#patients_form')
      form1.keypress(function (e) {
        if (e.which == 13) {
          return false;
        }
      });
      var data = form1.data();
      var _token = $("input[name=_token]").val();
      form1.validate({
        errorElement: 'span', // default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: '', // validate all fields including form hidden inpu
        messages: {
          name: {
            required: 'Campo obligatorio'
          },
          last_name: {
            required: 'Campo obligatorio'
          },
          type_document: {
            required: 'Compo obligatorio'
          },
          num_document: {
            required: 'Campo obligatorio',
            remote: 'Documento ya esta registrado'
          },
          email: {
            required: 'Campo obligatorio'
          },
          gender: {
            required: 'Campo obligatorio'
          },
          address: {
            required: 'Campo obligatorio'
          },
          birthdate: {
            required: 'Campo obligatorio'
          },
          civil_status: {
            required: 'Campo obligatorio'
          },
          mobile: {
            required: 'Campo obligatorio'
          },
          // phone: {
          //   required: 'Campo obligatorio'
          // },
          // occupation: {
          //   required: 'Campo obligatorio'
          // },
          // workplace: {
          //   required: 'Campo obligatorio'
          // }
        },
        rules: {
          name: {
            required: true
          },
          last_name: {
            required: true
          },
          type_document: {
            required: true
          },
          num_document: {
            required: true,
            digits: true,
            remote: {
              url: data.doc,
              type: 'post',
              data: {
                "_token": _token,
                model: data.model
              }
            }
          },
          email: {
            required: true
          },
          gender: {
            required: true
          },
          address: {
            required: true
          },
          birthdate: {
            required: true
          },
          civil_status: {
            required: true
          },
          mobile: {
            required: true
          },
          // phone: {
          //   required: true
          // },
          // occupation: {
          //   required: true
          // },
          // workplace: {
          //   required: true
          // }
        },

        errorPlacement: function (error, element) {
          // render error placement for each input type
          var cont = $(element).parent('.validate')

          if (cont) {
            cont.after(error)
          } else {
            element.after(error)
          }
        },

        highlight: function (element) {
          // entradas de error de hightlight
          $(element)
            .closest('.form-group')
            .removeClass('has-success')
            .addClass('has-error') // set error class to the control group
        },

        unhighlight: function (element) {
          // revertir el cambio realizado por hightlight
          $(element)
            .closest('.form-group')
            .removeClass('has-error')
            .addClass('has-success') // set error class to the control group
        },
        //
        success: function (label) {
          label
            .closest('.form-group')
            .removeClass('has-error')
            .addClass('has-success') // set success class to the control group
        }
      })


    }
  }
})();
var patient_delete = (function () {
  return {
    init: function () {
      // SweetAlert Eliminar
      var $delete = $('.btn-delete')
      var $table = $('.table-model')
      $delete.click(function (e) {
        var $data = $(this).data()
        e.preventDefault()
        swal(
          {
            title: '¿Esta seguro de Eliminar?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info btn-fill',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar',
            cancelButtonClass: 'btn btn-danger btn-fill',
            closeOnConfirm: false,
            allowOutsideClick: false
          },
          function () {
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              type: 'POST',
              dataType: 'json',
              url: $data.url,
              async: false,
              data: { id: $data.id },
              success: function (data) {
                if (data.status == 200) {
                  swal('¡Eliminado!', 'Fue eliminado con exito', 'success')
                  $('tr[data-id=' + $data.id + ']', $table).remove()
                  setTimeout(function () {
                    window.location.reload()
                  }, 2000)
                } else swal('Error al eliminar', data.message, 'error')
              }
            })
          }
        )
      })
    }
  }
})();


//crud doctors
var doctor_createAndEdit = (function () {
  return {
    init: function () {
      //select cambios de tipo documento
      $("select[id=type_document]").change(function () {
        var id = $('select[id=type_document]').val();
        var num_document = document.getElementById('num_document')
        var last_name = document.getElementById('last_name')
        var name = document.getElementById('name')

        if (id == 0) {
          activeInput(true);
        } else if (id == 1) {
          activeInput(false);
          num_document.setAttribute('maxlength', 8)
          num_document.value = ''
          last_name.value = ''
          name.value = ''
          num_document.setAttribute('placeHolder', 'Ingrese DNI')
          buscarSelect('Seleccionado', 'gender')
        } else {
          activeInput(false);
          num_document.setAttribute('maxlength', 12)
          num_document.value = ''
          last_name.value = ''
          name.value = ''
          num_document.setAttribute('placeHolder', 'Ingrese Pasaporte')
          buscarSelect('Seleccionado', 'gender')
        }
      });

      //verificamos si se ha seleccionado es decir cuando editamos 
      //y cuando agregamos aparecera como num_documento (0)
      var id_numDocument = $('select[id=type_document]').val();
      if (id_numDocument == 0) {
        activeInput(true);
      } else {
        activeInput(false);
      }

      var numDocument = $('#num_document')
      numDocument.keypress(function (event) {
        var keycode = event.keyCode ? event.keyCode : event.which
        if (keycode == '13') {
          // verificamos si es DNI o Pasaporte
          if (type_document.value == '1') {
            var num_document = document.getElementById('num_document').value
            var btn_SearchDniDoctor = document.getElementById('btn_SearchDniDoctor')
            var btn_SearchDniDoctor = btn_SearchDniDoctor.getAttribute('href')
            if (num_document.length == 8) {
              $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                    'content'
                  )
                },
                type: 'GET',
                dataType: 'json',
                url: btn_SearchDniDoctor,
                async: false,
                data: { dni: num_document },
                success: function (resultado) {
                  if (resultado.state == 'Correcto') {
                    document.getElementById('name').value = ucwords(resultado.nombre.toLowerCase());
                    document.getElementById('last_name').value =
                      ucwords(resultado.paterno.toLowerCase() + ' ' + resultado.materno.toLowerCase());
                    document.getElementById('last_name').value =
                      ucwords(resultado.paterno.toLowerCase() + ' ' + resultado.materno.toLowerCase());
                    document.getElementById('birth_date_doctors').value = convertDateFormat(resultado.nacimiento);
                    buscarSelect(resultado.sexo, 'gender')
                  } else {
                    swal(
                      'DNI incorrecto',
                      'Por favor verifique el dni ingresado y vuelva a intentar',
                      'error'
                    )
                    document.getElementById('num_document').value = ''
                    document.getElementById('last_name').value = ''
                    document.getElementById('name').value = ''
                    buscarSelect('Seleccionado', 'gender')
                  }
                }
              })
            }
          }
        }
      })
      function ucwords(str) {
        return (str + '')
          .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
            return $1.toUpperCase();
          });
      }

      $("#btn_buscar").click(function () {
        document.getElementById('img_loading_doctor').style.display = "inline";

        var funcion = function () {
          var num_document = document.getElementById('num_document').value
          var btn_SearchDniDoctor = document.getElementById('btn_SearchDniDoctor')
          var btn_SearchDniDoctor = btn_SearchDniDoctor.getAttribute('href')

          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                'content'
              )
            },
            type: 'GET',
            dataType: 'json',
            url: btn_SearchDniDoctor,
            async: false,
            data: { dni: num_document },
            success: function (resultado) {
              document.getElementById('img_loading_doctor').style.display = "none";
              if (resultado.state == 'Correcto') {
                document.getElementById('name').value = ucwords(resultado.nombre.toLowerCase());
                document.getElementById('last_name').value =
                  ucwords(resultado.paterno.toLowerCase() + ' ' + resultado.materno.toLowerCase());
                document.getElementById('last_name').value =
                  ucwords(resultado.paterno.toLowerCase() + ' ' + resultado.materno.toLowerCase());
                document.getElementById('birth_date_doctors').value = convertDateFormat(resultado.nacimiento);
                buscarSelect(resultado.sexo, 'gender')
              } else {
                swal(
                  'DNI incorrecto',
                  'Por favor verifique el dni ingresado y vuelva a intentar',
                  'error'
                )
                document.getElementById('num_document').value = ''
                document.getElementById('last_name').value = ''
                document.getElementById('name').value = ''
                buscarSelect('Seleccionado', 'gender')
              }
            }
          })
        }
        setTimeout(funcion, 2000);
      });



      //function para activar las casillas 
      function activeInput(state) {
        $('#num_document').prop("disabled", state);
        $('#name').prop("disabled", state);
        $('#last_name').prop("disabled", state);
        $('#gender').prop("disabled", state);
        $('#address').prop("disabled", state);
        $('#email').prop("disabled", state);
        $('#civil_status').prop("disabled", state);
        $('#mobile').prop("disabled", state);
        $('#phone').prop("disabled", state);
        $('#occupation').prop("disabled", state);
        $('#workplace').prop("disabled", state);
        $('#birth_date_doctors').prop("disabled", state);
        $('#code').prop("disabled", state);
        $('#standbyTime').prop("disabled", state);
        $('#btn-add').prop("disabled", state);
        $('#btn-cancel').prop("disabled", state);
        $('#upload_image').prop("disabled", state);
        $('#specialty_id').prop("disabled", state);
        $('#civil_status').prop("disabled", state);
        if (state == false) {
          $('#btn_buscar').css("display", "inline");
        } else {
          $('#btn_buscar').css("display", "none");
        }
      }
    }
  }
})();

var doctor_form = (function () {
  return {
    init: function () {
      // Validaciones
      var form1 = $('#doctors_form');
      var data = form1.data();
      var _token = $("input[name=_token]").val();
      form1.keypress(function (e) {
        if (e.which == 13) {
          return false;
        }
      });
      // form1.validate({
      //   errorElement: 'span', // default input error message container
      //   errorClass: 'help-block help-block-error', // default input error message class
      //   focusInvalid: true, // do not focus the last invalid input
      //   ignore: '', // validate all fields including form hidden inpu
      //   messages: {
      //     name: {
      //       required: 'Campo obligatorio'
      //     },
      //     last_name: {
      //       required: 'Campo obligatorio'
      //     },
      //     type_document: {
      //       required: 'Campo obligatorio'
      //     },
      //     num_document: {
      //       required: 'Campo obligatorio',
      //       remote: 'Documento ya esta registrado'
      //     },
      //     email: {
      //       required: 'Campo obligatorio'
      //     },
      //     gender: {
      //       required: 'Campo obligatorio'
      //     },
      //     address: {
      //       required: 'Campo obligatorio'
      //     },
      //     birthdate: {
      //       required: 'Campo obligatorio'
      //     },
      //     civil_status: {
      //       required: 'Campo obligatorio'
      //     },
      //     mobile: {
      //       required: 'Campo obligatorio'
      //     },
      //     phone: {
      //       required: 'Campo obligatorio'
      //     },
      //     code: {
      //       required: 'Campo obligatorio'
      //     },
      //     specialty_id: {
      //       required: 'Campo obligatorio'
      //     }
      //   },
      //   rules: {
      //     name: {
      //       required: true
      //     },
      //     last_name: {
      //       required: true
      //     },
      //     type_document: {
      //       required: true
      //     },
      //     num_document: {
      //       required: true,
      //       digits: true,
      //       remote: {
      //         url: data.doc,
      //         type: 'post',
      //         data: {
      //           "_token": _token,
      //           model: data.model
      //         }
      //       }
      //     },
      //     email: {
      //       required: true
      //     },
      //     gender: {
      //       required: true
      //     },
      //     address: {
      //       required: true
      //     },
      //     birthdate: {
      //       required: true
      //     },
      //     civil_status: {
      //       required: true
      //     },
      //     mobile: {
      //       required: true
      //     },
      //     phone: {
      //       required: true
      //     },
      //     code: {
      //       required: true
      //     },
      //     specialty_id: {
      //       required: true
      //     }
      //   },
      //
      //   errorPlacement: function (error, element) {
      //     // render error placement for each input type
      //     var cont = $(element).parent('.validate')
      //
      //     if (cont) {
      //       cont.after(error)
      //     } else {
      //       element.after(error)
      //     }
      //   },
      //
      //   highlight: function (element) {
      //     // entradas de error de hightlight
      //     $(element)
      //       .closest('.form-group')
      //       .removeClass('has-success')
      //       .addClass('has-error') // set error class to the control group
      //   },
      //
      //   unhighlight: function (element) {
      //     // revertir el cambio realizado por hightlight
      //     $(element)
      //       .closest('.form-group')
      //       .removeClass('has-error')
      //       .addClass('has-success') // set error class to the control group
      //   },
      //   //
      //   success: function (label) {
      //     label
      //       .closest('.form-group')
      //       .removeClass('has-error')
      //       .addClass('has-success') // set success class to the control group
      //   }
      // })
    }
  }
})();

var doctor_delete = (function () {
  return {
    init: function () {
      // SweetAlert Eliminar
      var $delete = $('.btn-delete')
      var $table = $('.table-model')
      $delete.click(function (e) {
        var $data = $(this).data()
        e.preventDefault()
        swal(
          {
            title: '¿Esta seguro de Eliminar?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-info btn-fill',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar',
            cancelButtonClass: 'btn btn-danger btn-fill',
            closeOnConfirm: false,
            allowOutsideClick: false
          },
          function () {
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              type: 'POST',
              dataType: 'json',
              url: $data.url,
              async: false,
              data: { id: $data.id },
              success: function (data) {
                if (data.status == 200) {
                  swal('¡Eliminado!', 'Fue eliminado con exito', 'success')
                  $('tr[data-id=' + $data.id + ']', $table).remove()
                  setTimeout(function () {
                    window.location.reload()
                  }, 2000)
                } else swal('Error al eliminar', data.message, 'error')
              }
            })
          }
        )
      })
    }
  }
})();


//header

var header = (function () {
  return {
    init: function () {
      // SweetAlert Eliminar
      var count = 0;
      var funcion = function () {
        console.log("paso");
        var btn_verificateAppointments = document.getElementById('btn_verificateAppointments')
        var btn_verificateAppointments = btn_verificateAppointments.getAttribute('href')
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: 'GET',
          dataType: 'json',
          url: btn_verificateAppointments,
          async: false,
          data: {
            search: "1"
          },
          success: function (resultado) {
            var notify1 = 0;

            if (resultado.state == "Incorrecto") {
              document.getElementById('id_notify1_appointments').innerHTML = 0;
              document.getElementById('id_notify2_appointments').innerHTML = "Nuevos 0";
            } else {

              var id_container_appointments = document.getElementById('id_container_appointments');
              id_container_appointments.innerHTML = "";
              resultado.appointments.forEach(function (element) {
                var estado = "";
                if (element.view_click == "0") {
                  notify1 = parseInt(notify1) + 1;
                  estado = "Sin Ver";
                } else {
                  estado = "Visto";
                }

                id_container_appointments.innerHTML += `<li onclick="loadPatient('${element.slug_patient}','${element.id_patient}')">
                            <div class="prog-avatar">
                                <img src="/element/images/doctors/default.png" alt="" width="40" height="40">
                            </div>
                            <div class="details">
                                <div class="title">                                
                                    <a >${element.patient_name}</a> -( ${element.hour})
                                </div>
                                <div>
                                    <span class="clsAvailable">${estado}</span>
                                </div>
                            </div>
                        </li>`
              });
            }

            document.getElementById('id_notify1_appointments').innerHTML = notify1;
            document.getElementById('id_notify2_appointments').innerHTML = "Nuevos " + notify1;




          }
        })
      }
      function loadPatient(slug_patient, id_patient) {
        var id_patient_viewClick = document.getElementById('id_patient_viewClick');
        var id_patient_viewClick = id_patient_viewClick.getAttribute('href')
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: 'GET',
          dataType: 'json',
          url: id_patient_viewClick,
          async: false,
          data: {
            id: id_patient
          },
          success: function (resultado) {
            console.log(resultado);
            var id_patient_edit = document.getElementById('id_patient_edit');
            var id_patient_edit = id_patient_edit.getAttribute('href')
            id_patient_edit = id_patient_edit.substring(0, id_patient_edit.length - 1)
            id_patient_edit = id_patient_edit + '' + slug_patient;
            document.getElementById('id_patient_edit').href = id_patient_edit;
            document.getElementById('id_patient_edit').click();


          }
        })
      }
      //cuando inicia por primera vez se tiene que ejecutar al segundo y despues si a los 
      setInterval(funcion, 1000);
    }
  }
})();


//programmer
var programmer_form = (function () {
  return {
    init: function () {
      // Validaciones
      var form1 = $('#form_programmer')
      var data = form1.data();
      var _token = $("input[name=_token]").val();
      form1.validate({
        errorElement: 'span', // default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: '', // validate all fields including form hidden inpu
        messages: {
          sms_doctor_register: {
            required: 'Campo obligatorio'
          },
          sms_patient_register: {
            required: 'Campo obligatorio'
          },
          sms_patient_appointment: {
            required: 'Compo obligatorio'
          },
          email_patient_appointment: {
            required: 'Campo obligatorio',
          },
          email_doctor_register: {
            required: 'Campo obligatorio'
          }
        },
        rules: {
          sms_doctor_register: {
            required: true
          },
          sms_patient_register: {
            required: true
          },
          sms_patient_appointment: {
            required: true
          },
          email_patient_appointment: {
            required: true
          },
          email_doctor_register: {
            required: true
          }
        },

        errorPlacement: function (error, element) {
          // render error placement for each input type
          var cont = $(element).parent('.validate')

          if (cont) {
            cont.after(error)
          } else {
            element.after(error)
          }
        },

        highlight: function (element) {
          // entradas de error de hightlight
          $(element)
            .closest('.form-group')
            .removeClass('has-success')
            .addClass('has-error') // set error class to the control group
        },

        unhighlight: function (element) {
          // revertir el cambio realizado por hightlight
          $(element)
            .closest('.form-group')
            .removeClass('has-error')
            .addClass('has-success') // set error class to the control group
        },
        //
        success: function (label) {
          label
            .closest('.form-group')
            .removeClass('has-error')
            .addClass('has-success') // set success class to the control group
        }
      })
    }
  }
})()


