$(document).ready(function()
		{
			$('#birth_date_patients').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				lang: 'es'
			});
			$('#birth_date_doctors').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true,
				lang: 'es'
			});
			$('#date1').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true
			});
			$('#date2').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true
			});
			$('#dateOfBirth').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true
			});

			$('#therapyDate').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true
			});
			
			$('#time').bootstrapMaterialDatePicker
			({
				date: false,
				shortTime: false,
				format: 'HH:mm'
			});

			$('#start-time-attention').bootstrapMaterialDatePicker
			({
				date: false,
				shortTime: true,
				twelvehour: true,
				format: 'HH:mm a'
			})


			$('#end-time-attention').bootstrapMaterialDatePicker
			({
				date: false,
				shortTime: true,
				twelvehour: true,
				format: 'HH:mm a'
			});

			$('#date-format').bootstrapMaterialDatePicker
			({
				format: 'dddd DD MMMM YYYY - HH:mm'
			});
			$('#date-fr').bootstrapMaterialDatePicker
			({
				format: 'DD/MM/YYYY HH:mm',
				lang: 'fr',
				weekStart: 1, 
				cancelText : 'ANNULER',
				nowButton : true,
				switchOnClick : true
			});

			

			$('#date-end').bootstrapMaterialDatePicker
			({
				weekStart: 0, format: 'MM/DD/YYYY HH:mm'
			});
			$('#date-start').bootstrapMaterialDatePicker
			({
				weekStart: 0, format: 'MM/DD/YYYY HH:mm',shortTime : true
			}).on('change', function(e, date)
			{
				$('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
			});

			$('#min-date').bootstrapMaterialDatePicker({ format : 'DD/MM/YYYY HH:mm', minDate : new Date() });

			
		});
